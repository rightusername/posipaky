import webpack from 'webpack';
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import autoprefixer from 'autoprefixer';
import postcssNext from 'postcss-cssnext';
import postcssImport from 'postcss-import';
import postcssExtend from 'postcss-extend';
import postcssReporter from 'postcss-reporter';
import StyleLintPlugin from 'stylelint-webpack-plugin';
import MinifyPlugin from 'babel-minify-webpack-plugin';
var CopyWebpackPlugin = require('copy-webpack-plugin');
import fs from 'fs';

const extractStyles = new ExtractTextPlugin({ filename: 'css/[name].css' });

const supportedBrowsers = [
  '> 0.5%',
  'last 2 versions',
  'not ie <= 10',
];

const postcssProcessors = [
  postcssImport,
  postcssExtend,
  postcssNext({ browsers: supportedBrowsers }),
  postcssReporter({ clearReportedMessages: true }),
];

const scssProcessors = [
  autoprefixer({
    browsers: supportedBrowsers,
    cascade: false,
  }),
  postcssReporter({ clearReportedMessages: true }),
];

function generateHtmlPlugins(templateDir) {
  const templateFiles = fs.readdirSync(path.join(__dirname, templateDir));

  let plugins = [];
  for (let i = 0; i < templateFiles.length; i++) {
    if(templateFiles[i].match('.html')) {
      plugins.push(new HtmlWebpackPlugin({
        filename: templateFiles[i],
        inject: false,
        template: path.join(__dirname, templateDir, templateFiles[i])
      }))
    }
  }
  return plugins;
}

const htmlPlugins = generateHtmlPlugins('./src')

module.exports = () => {
  const stylesType = process.env.STYLES; // postcss or scss
  const stylesExtension = stylesType === 'scss' ? '.scss' : '.css';

  return {
    context: path.resolve(__dirname, 'src'),

    entry: {
      main: './app.js',
      deputat: './js/deputat.js'
    },

    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'js/[name].js',
    },

    watch: false,
    
    devtool: false,

    module: {
      rules: [
        {
          test: /\.js$/,
          include: path.resolve(__dirname, 'src/js'),
          use: [
            {
              loader: 'babel-loader',
              options: {
                cacheDirectory: true,
                plugins: ['transform-runtime'],
              },
            }
          ],
        },
        {
          test: /\.css$/,
          use: extractStyles.extract({
            use: [
              {
                loader: 'css-loader',
                options: {
                  minimize: true,
                },
              },
              {
                loader: 'postcss-loader',
                options: {
                  plugins: (loader) => postcssProcessors,
                },
              },
            ],
            publicPath: '../',
          }),
        },
        {
          test: /\.scss$/,
          use: extractStyles.extract({
            use: [
              {
                loader: "css-loader",
                options: {
                  minimize: true,
                },
              },
              {
                loader: 'postcss-loader',
                options: {
                  plugins: (loader) => scssProcessors,
                },
              },
              {
                loader: "sass-loader",
              },
            ],
            publicPath: '../',
          }),
        },
        {
          test: /.*\.(gif|png|jpe?g|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'assets/[name].[ext]',
              },
            },
            {
              loader: 'image-webpack-loader',
              options: {
                pngquant: {
                  quality: '85-90',
                  speed: 4,
                },
                mozjpeg: {
                  quality: 85,
                  progressive: true,
                },
                gifsicle: {
                  interlaced: true,
                },
              },
            },
          ],
        },

        {
          test: /\.(woff2?|ttf|otf|eot)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'assets/[name].[ext]',
              },
            },
          ],
        }
      ]
    },

    plugins: [
      new webpack.DefinePlugin({
        LANG: JSON.stringify("en"),
        "process.env": { NODE_ENV: "'production'" },
      }),

      new CopyWebpackPlugin([
        { from: 'assets', to: 'assets' },
        { from: 'static', to: 'static' },
        {
          from: '*.php',
          to: '[name].[ext]'
        }
      ]), 


      ...htmlPlugins,


      extractStyles,

      new MinifyPlugin(),
      new webpack.optimize.UglifyJsPlugin({
        minimize: true
      })
    ]
  }
};
