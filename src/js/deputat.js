(function () {
    let dropdown = document.getElementById('drop_down');
    let showmore = document.getElementById('show_more');
    let panewraps = document.querySelectorAll('div.bottom_block.list_persons.cf div.wrap_block.cf');

    let btn = document.createElement('div');
    
    if (dropdown) {
        btn.classList.add('drop_down_open');
        btn.innerHTML = dropdown.querySelector('li.active a').innerHTML;
        btn.onclick = () => {
            if(dropdown.classList.contains('open')) {
                dropdown.classList.remove('open');
                btn.classList.remove('open');
            } else {
                dropdown.classList.add('open');
                btn.classList.add('open');
            }
        }
        dropdown.parentNode.insertBefore(btn, dropdown);
        for (let i = 0; i < dropdown.children.length; i++) {
            dropdown.children[i].onclick = () => {
                btn.innerHTML = dropdown.children[i].innerText;
                dropdown.classList.remove('open');
            }
        }

    }

    if(showmore){

        showmore.onclick = () => {
            for (let i = 0; i < panewraps.length; i++) {
                if (panewraps[i].classList.contains('open')) {
                    panewraps[i].classList.remove('open');
                    showmore.classList.remove('open');
                    showmore.innerHTML = "Показати більше";
                } else {
                    panewraps[i].classList.add('open');
                    showmore.classList.add('open');
                    showmore.innerHTML = "Показати менше";
                }
                
            }
        }
    }

    

    

})();