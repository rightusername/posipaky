import AOS from 'aos'

(function () {

    AOS.init({
        duration: 700,
        once: true
    });

})();